#!/bin/bash
rm -rf control-ip.sh &>/dev/null
rm -rf install-master.sh &>/dev/null
rm -rf master.sh &>/dev/null
clear
# INSTALACAO BASICA
[[ -e /etc/newadm-instalacao ]] && BASICINST="$(cat /etc/newadm-instalacao)" || BASICINST="ADMbot.sh C-SSR.sh Crear-Demo.sh PDirect.py PGet.py POpen.py PPriv.py PPub.py Shadowsocks-R.sh Shadowsocks-libev.sh Unlock-Pass-VULTR.sh apacheon.sh blockBT.sh budp.sh dns-netflix.sh dropbear.sh fai2ban.sh gestor.sh menu message.txt openvpn.sh paysnd.sh ports.sh shadowsocks.sh sockspy.sh speed.sh speedtest.py squid.sh squidpass.sh ssl.sh tcp.sh ultrahost usercodes utils.sh v2ray.sh install-ddos.sh"
IVAR="/etc/http-instas"
system=$(cat /etc/issue.net)
ar=$(wget -qO- ipv4.icanhazip.com)
credito=$(cat /etc/SCRIPT/message.txt)
mostrariv=$(cat /etc/http-instas)
IP=$(wget -qO- ipv4.icanhazip.com)
BARRA="\033[1;33m⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊\033[0m"
BARRA1="\033[1;37m\033[0m"
_hora=$(printf '%(%H:%M:%S)T') 
_fecha=$(printf '%(%D)T') 
#PROCESADOR
_core=$(printf '%-1s' "$(grep -c cpu[0-9] /proc/stat)")
_usop=$(printf '%-1s' "$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')")

#SISTEMA-USO DA CPU-MEMORIA RAM
ram1=$(free -h | grep -i mem | awk {'print $2'})
ram2=$(free -h | grep -i mem | awk {'print $4'})
ram3=$(free -h | grep -i mem | awk {'print $3'})

_ram=$(printf ' %-9s' "$(free -h | grep -i mem | awk {'print $2'})")
_usor=$(printf '%-8s' "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')")

if [[ -e /etc/bash.bashrc-bakup ]]; then AutoRun="\033[1;32m[ON]"
elif [[ -e /etc/bash.bashrc ]]; then AutoRun="\033[1;31m[OFF]"
fi

echo -e "\033[1;33m⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊⚊\033[1;30m"
figlet -p -f slant < /root/name
echo -e "$BARRA"
echo -e "\033[1;37m            🔰 PANEL GENERADOR DE KEYS 🔰        \033[0m"
echo -e "$BARRA"
echo -e "         \033[101;1;37mLA IP $IP ESTA AUTORIZADA\033[0m"
SCPT_DIR="/etc/SCRIPT"
[[ ! -e ${SCPT_DIR} ]] && mkdir ${SCPT_DIR}
INSTA_ARQUIVOS="ADMVPS.zip"
DIR="/etc/http-shell"
LIST="xMKmdA" 
memorias () {
if [[ "$(grep -c "Ubuntu" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f2 /etc/issue.net |awk -F "." '{print $1}')
elif [[ "$(grep -c "Debian" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f3 /etc/issue.net)
else
system=$(cut -d' ' -f1 /etc/issue.net)
fi
_ram=$(printf ' %-9s' "$(free -h | grep -i mem | awk {'print $2'})")
_usor=$(printf '%-8s' "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')")
_usop=$(printf '%-1s' "$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')")
_core=$(printf '%-1s' "$(grep -c cpu[0-9] /proc/stat)")
_system=$(printf '%-14s' "$system")
_hora=$(printf '%(%H:%M:%S)T')
echo -e "\033[1;37mSISTEMA            MEMORIA RAM      PROCESSADOR \033[0m"
echo -e "\033[1;37mOS: \033[1;33m$_system \033[1;37mTotal:\033[1;33m$_ram \033[1;37mNucleos: \033[1;33m$_core\033[0m"
echo -e "\033[1;37mHora:\033[1;33m $_hora     \033[1;37mEn uso: \033[1;33m$_usor \033[1;37mEn uso: \033[1;33m$_usop\033[0m"
}
meu_ip () {
MIP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
MIP2=$(wget -qO- ipv4.icanhazip.com)
[[ "$MIP" != "$MIP2" ]] && IP="$MIP2" || IP="$MIP"
}
fun_list () {
rm ${SCPT_DIR}/*.x.c &> /dev/null
unset KEY
KEY="$1"
#CRIA DIR
[[ ! -e ${DIR} ]] && mkdir ${DIR}
#ENVIA ARQS
i=0
VALUE+="gerar.sh instgerador.sh http-server.py $BASICINST"
for arqx in `ls ${SCPT_DIR}`; do
[[ $(echo $VALUE|grep -w "${arqx}") ]] && continue 
echo -e "[$i] -> ${arqx}"
arq_list[$i]="${arqx}"
let i++
done
clear
echo -e "$BARRA"
echo -e "\033[1;33mPANEL GENERADOR DE KEY\033[0m"
echo -e "$BARRA"
echo -e "\033[1;36m[b] \033[1;32m› \033[1;37mKEY PARA PANEL ADM-MX \033[0m"
echo -e "$BARRA"
read -p "Seleccione los archivos a instalar: " -e -i b readvalue
#CRIA KEY
[[ ! -e ${DIR}/${KEY} ]] && mkdir ${DIR}/${KEY}
#PASSA ARQS
[[ -z $readvalue ]] && readvalue="b"
read -p "Nombre de usuario ( comprador de la key ): " nombrevalue
[[ -z $nombrevalue ]] && nombrevalue="SIN NOMBRE"
read -p "Key Fija? [S/N]: " -e -i n fixakey
[[ $fixakey = @(s|S|y|Y) ]] && read -p "Digita la IP FIJA: " IPFIX && nombrevalue+=[FIXA]
if [[ $readvalue = @(b|B) ]]; then
#ADM BASIC
 arqslist="$BASICINST"
 for arqx in `echo "${arqslist}"`; do
 [[ -e ${DIR}/${KEY}/$arqx ]] && continue #ANULA ARQUIVO CASO EXISTA
 cp ${SCPT_DIR}/$arqx ${DIR}/${KEY}/
 echo "$arqx" >> ${DIR}/${KEY}/${LIST}
 done
elif [[ $readvalue = @(x|X) ]]; then
read -p "KEY DE ACTUALIZACIÓN?: [Y/N]: " -e -i n attGEN
[[ $(echo $nombrevalue|grep -w "FIXA") ]] && nombrevalue+=[GERADOR]
 for arqx in `ls $SCPT_DIR`; do
  [[ -e ${DIR}/${KEY}/$arqx ]] && continue #ANULA ARQUIVO CASO EXISTA
  cp ${SCPT_DIR}/$arqx ${DIR}/${KEY}/
 echo "$arqx" >> ${DIR}/${KEY}/${LIST}
 echo "Gerador" >> ${DIR}/${KEY}/GERADOR
 done
if [[ $attGEN = @(Y|y|S|s) ]]; then
[[ -e ${DIR}/${KEY}/gerar.sh ]] && rm ${DIR}/${KEY}/gerar.sh
[[ -e ${DIR}/${KEY}/http-server.py ]] && rm ${DIR}/${KEY}/http-server.py
fi
else
 for arqx in `echo "${readvalue}"`; do
 #UNE ARQ
 [[ -e ${DIR}/${KEY}/${arq_list[$arqx]} ]] && continue #ANULA ARQUIVO CASO EXISTA
 rm ${SCPT_DIR}/*.x.c &> /dev/null
 cp ${SCPT_DIR}/${arq_list[$arqx]} ${DIR}/${KEY}/
 echo "${arq_list[$arqx]}" >> ${DIR}/${KEY}/${LIST}
 done
echo "TRUE" >> ${DIR}/${KEY}/FERRAMENTA
fi
rm ${SCPT_DIR}/*.x.c &> /dev/null
echo "$nombrevalue" > ${DIR}/${KEY}.name
[[ ! -z $IPFIX ]] && echo "$IPFIX" > ${DIR}/${KEY}/keyfixa
echo -e "$BARRA"
}

ofus () {
unset server
server=$(echo ${txt_ofuscatw}|cut -d':' -f1)
unset txtofus
number=$(expr length $1)
for((i=1; i<$number+1; i++)); do
txt[$i]=$(echo "$1" | cut -b $i)
case ${txt[$i]} in
".")txt[$i]="+";;
"+")txt[$i]=".";;
"1")txt[$i]="@";;
"@")txt[$i]="1";;
"2")txt[$i]="?";;
"?")txt[$i]="2";;
"5")txt[$i]="%";;
"%")txt[$i]="5";;
"-")txt[$i]="K";;
"K")txt[$i]="-";;
esac
txtofus+="${txt[$i]}"
done
echo "$txtofus" | rev
}

gerar_key () {
valuekey="$(date | md5sum | head -c10)"
valuekey+="$(echo $(($RANDOM*10))|head -c 5)"
fun_list "$valuekey"
keyfinal=$(ofus "$IP:8888/$valuekey/$LIST")
echo -e "\033[1;37mCopie su KEY\033[0m"
echo -e "KEY ➾ \033[47;91m$keyfinal\033[0m"
echo -e "$BARRA"
echo -e "SCRIPT"
echo -e "wget https://gitlab.com/reaper21delta/gamo/-/raw/master/adm-install.sh && chmod +x *.sh && ./adm-install.sh"
echo -e "$BARRA"
read -p "Enter para Finalizar"
}
att_gen_key () {
i=0
rm ${SCPT_DIR}/*.x.c &> /dev/null
[[ -z $(ls $DIR|grep -v "ERROR-KEY") ]] && return
echo "[$i] Retornar"
keys="$keys retorno"
let i++
for arqs in `ls $DIR|grep -v "ERROR-KEY"|grep -v ".name"`; do
arqsx=$(ofus "$IP:8888/$arqs/$LIST")
if [[ $(cat ${DIR}/${arqs}.name|grep GERADOR) ]]; then
echo -e "\033[1;31m[$i] $arqsx ($(cat ${DIR}/${arqs}.name))\033[1;32m ($(cat ${DIR}/${arqs}/keyfixa))\033[0m"
keys="$keys $arqs"
let i++
fi
done
keys=($keys)
echo -e "$BARRA"
while [[ -z ${keys[$value]} || -z $value ]]; do
read -p "Seleccione qué Actualizar[t=todos]: " -e -i 0 value
done
[[ $value = 0 ]] && return
if [[ $value = @(t|T) ]]; then
i=0
[[ -z $(ls $DIR|grep -v "ERROR-KEY") ]] && return
for arqs in `ls $DIR|grep -v "ERROR-KEY"|grep -v ".name"`; do
KEYDIR="$DIR/$arqs"
rm $KEYDIR/*.x.c &> /dev/null
 if [[ $(cat ${DIR}/${arqs}.name|grep GERADOR) ]]; then #Keyen Atualiza
 rm ${KEYDIR}/${LIST}
   for arqx in `ls $SCPT_DIR`; do
    cp ${SCPT_DIR}/$arqx ${KEYDIR}/$arqx
    echo "${arqx}" >> ${KEYDIR}/${LIST}
    rm ${SCPT_DIR}/*.x.c &> /dev/null
    rm $KEYDIR/*.x.c &> /dev/null
   done
 arqsx=$(ofus "$IP:8888/$arqs/$LIST")
 echo -e "\033[1;33m[KEY]: $arqsx \033[1;32m(ACTUALIZADA!)\033[0m"
 fi
let i++
done
rm ${SCPT_DIR}/*.x.c &> /dev/null
echo -e "$BARRA"
echo -ne "\033[0m" && read -p "Enter"
return 0
fi
KEYDIR="$DIR/${keys[$value]}"
[[ -d "$KEYDIR" ]] && {
rm $KEYDIR/*.x.c &> /dev/null
rm ${KEYDIR}/${LIST}
  for arqx in `ls $SCPT_DIR`; do
  cp ${SCPT_DIR}/$arqx ${KEYDIR}/$arqx
  echo "${arqx}" >> ${KEYDIR}/${LIST}
  rm ${SCPT_DIR}/*.x.c &> /dev/null
  rm $KEYDIR/*.x.c &> /dev/null
  done
 arqsx=$(ofus "$IP:8888/${keys[$value]}/$LIST")
 echo -e "\033[1;33m[KEY]: $arqsx \033[1;32m(ACTUALIZADA!)\033[0m"
 read -p "Enter"
 rm ${SCPT_DIR}/*.x.c &> /dev/null
 }
}
remover_key () {
i=0
[[ -z $(ls $DIR|grep -v "ERROR-KEY") ]] && return
clear
echo -e "$BARRA"
echo -e "\033[1;33mPANEL DE ELIMINACIÓN\033[0m"
echo -e "$BARRA"
echo "[$i] Retornar"
echo -e "$BARRA"
keys="$keys retorno"
let i++
for arqs in `ls $DIR|grep -v "ERROR-KEY"|grep -v ".name"`; do
arqsx=$(ofus "$IP:8888/$arqs/$LIST")
if [[ ! -e ${DIR}/${arqs}/used.date ]]; then
echo -e "\033[1;32m[$i] \033[1;33m$arqsx\n                   \033[1;97m($(cat ${DIR}/${arqs}.name))\033[1;32m (ACTIVA)\033[0m\n$BARRA"
else
echo -e "\033[1;31m[$i] $arqsx\n  ($(cat ${DIR}/${arqs}.name))\033[1;33m ($(cat ${DIR}/${arqs}/used.date) IP: $(cat ${DIR}/${arqs}/used))\033[0m\n$BARRA"
fi
keys="$keys $arqs"
let i++
done
keys=($keys)
while [[ -z ${keys[$value]} || -z $value ]]; do
read -p "Elija cual eliminar: " -e -i 0 value
done
[[ -d "$DIR/${keys[$value]}" ]] && rm -rf $DIR/${keys[$value]}* || return
}
remover_key_usada () {
i=0
[[ -z $(ls $DIR|grep -v "ERROR-KEY") ]] && return
for arqs in `ls $DIR|grep -v "ERROR-KEY"|grep -v ".name"`; do
arqsx=$(ofus "$IP:8888/$arqs/$LIST")
 if [[ -e ${DIR}/${arqs}/used.date ]]; then #KEY USADA
  if [[ $(ls -l -c ${DIR}/${arqs}/used.date|cut -d' ' -f7) != $(date|cut -d' ' -f3) ]]; then
  rm -rf ${DIR}/${arqs}*
  echo -e "\033[1;31m[KEY]: $arqsx \033[1;32m(ELIMINADA!)\033[0m" 
  else
  echo -e "\033[1;32m[KEY]: $arqsx \033[1;32m(AÚN VÁLIDA!)\033[0m"
  fi
 else
 echo -e "\033[1;32m[KEY]: $arqsx \033[1;32m(AÚN VÁLIDA!)\033[0m"
 fi
let i++
done
echo -e "$BARRA"
echo -ne "\033[0m" && read -p "Enter"
}
atualizar_keyfixa () {
i=0
[[ -z $(ls $DIR|grep -v "ERROR-KEY") ]] && return
for arqs in `ls $DIR|grep -v "ERROR-KEY"|grep -v ".name"`; do
 if [[ $(cat ${DIR}/${arqs}.name|grep FIXA) ]]; then #Keyfixa Atualiza
   for arqx in `echo "${BASICINST}"`; do
    cp ${SCPT_DIR}/$arqx ${DIR}/${arqs}/$arqx
   done
 arqsx=$(ofus "$IP:8888/$arqs/$LIST")
 echo -e "\033[1;33m[KEY]: $arqsx \033[1;32m(ACTUALIZADA!)\033[0m"
 fi
let i++
done
echo -e "$BARRA"
echo -ne "\033[0m" && read -p "Enter"
}
start_gen () {
unset PIDGEN
PIDGEN=$(ps aux|grep -v grep|grep "http-server.sh")
if [[ ! $PIDGEN ]]; then
screen -dmS generador /bin/http-server.sh -start
# screen -dmS generador /bin/http-server-pass.sh -start
else
killall http-server.sh
# killall http-server-pass.sh
fi
}
rmv_iplib () {
echo -e "SERVIDORES DE KEY ACTIVOS!"
rm /var/www/html/newlib && touch /var/www/html/newlib
rm ${SCPT_DIR}/*.x.c &> /dev/null
[[ -z $(ls $DIR|grep -v "ERROR-KEY") ]] && return
for arqs in `ls $DIR|grep -v "ERROR-KEY"|grep -v ".name"`; do
if [[ $(cat ${DIR}/${arqs}.name|grep GERADOR) ]]; then
var=$(cat ${DIR}/${arqs}.name)
ip=$(cat ${DIR}/${arqs}/keyfixa)
echo -ne "\033[1;31m[USUARIO]:(\033[1;32m${var%%[*}\033[1;31m) \033[1;33m[GERADOR]:\033[1;32m ($ip)\033[0m"
echo "$ip" >> /var/www/html/newlib && echo -e " \033[1;36m[ATUALIZADO]"
fi
done
echo "104.238.135.147" >> /var/www/html/newlib
echo -e "$BARRA"
read -p "Enter"
}
atualizar_geb () {
clear
echo -e "$BARRA"
echo -e "\033[1;33m ⚠️ ACTUALIZADOR ⚠️     \033[0m"
echo -e "$BARRA"
echo -e "\033[37mDesea continuar?\033[0m"
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p " [S/N]: " yesno
tput cuu1 && tput dl1
done
if [[ ${yesno} = @(s|S|y|Y) ]]; then
wget -O $HOME/act.sh https://www.dropbox.com/s/e1g2sjmzxyqpj8e/install-master.sh &>/dev/null
chmod +x $HOME/act.sh
cd $HOME
./act.sh
rm $HOME/act.sh &>/dev/null
else
echo -e "\033[1;31mProcedimiento Cancelado\033[0m"
echo -e "$barra"
fi
}
uninstal_master () {
clear
echo -e "$BARRA"
echo -e "\033[1;33m ⚠️ AVISO IMPORTANTE ⚠️     \033[0m"
echo -e "\033[33mESTE PROCESO NO ES REVERSIBLE\033[0m"
echo -e "$BARRA"
echo -e "\033[37mDesea continuar?\033[0m"
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p " [S/N]: " yesno
tput cuu1 && tput dl1
done
if [[ ${yesno} = @(s|S|y|Y) ]]; then
[[ ! -d ${SCPT_DIR} ]] && rm -rf ${SCPT_DIR}
  rm -rf http-server.sh
  rm -rf lista-arq
  killall http-server.sh &>/dev/null
rm -rf /bin/http-server.sh
rm -rf /usr/bin/gerar.sh
rm -rf /etc/http-instas
rm -rf /etc/key-gerador
else
echo -e "\033[1;31mProcedimiento Cancelado\033[0m"
echo -e "$barra"
fi
}
ourscript () {
read -p "NUEVO MENSAJE: " MSGNEW
echo "$MSGNEW" > ${SCPT_DIR}/message.txt
echo -e "$BARRA"
}
bot_menu () {

CIDdir=/etc/ADM-db && [[ ! -d ${CIDdir} ]] && mkdir ${CIDdir}

[[ ! -e "${CIDdir}/confbot.sh" ]] && wget -O ${CIDdir}/confbot.sh https://www.dropbox.com/s/a8dmb1pfryhnvia/confbot.sh &> /dev/null && chmod +x ${CIDdir}/confbot.sh

source ${CIDdir}/confbot.sh

bot_conf

}
meu_ip
unset PID_GEN
PID_GEN=$(ps x|grep -v grep|grep "http-server.sh")
[[ ! $PID_GEN ]] && PID_GEN="\033[1;31m[ OFF ]" || PID_GEN="\033[1;32m[ ON ]"
echo -e "$BARRA"
memorias
echo -e "$BARRA"
echo -e "\033[1;37mReseller : \033[1;44m$credito \033[0m"
echo -e "\033[1;37mKeys Instaladas : \033[1;37m[\033[1;32m $mostrariv \033[1;37m]\033[0m"
echo -e "$BARRA"
echo -e "\e[1;37m[1] \033[1;32m› \033[1;37mGENERAR 1 KEY ALEATORIA"
echo -e "\e[1;37m[2] \033[1;32m› \033[1;37mELIMINAR/MIRAR KEYS FIJAS"
echo -e "\e[1;37m[3] \033[1;32m› \033[1;37mVER LOG DE \033[1;34mKEYS FIJAS"
echo -e "\e[1;37m[4] \033[1;32m› \033[1;37mLIMPIAR REGISTRO DE KEYS USADAS"
echo -e "\e[1;37m[5] \033[1;32m› \033[1;37mENCENDER/APAGAR GENERADOR $PID_GEN \033[0m"
echo -e "\e[1;37m[6] \033[1;32m› \033[1;37mACTUALIZAR KEY FIJA"
echo -e "\e[1;37m[7] \033[1;32m› \033[1;37mACTUALIZAR GENERADOR"
echo -e "\e[1;37m[8] \033[1;32m› \033[1;37mDESINSTALAR GENERADOR"
echo -e "\e[1;37m[9] \033[1;32m› \033[1;33mPRUEBA BOT"
echo -e "\e[1;37m[0] \033[1;32m› \033[1;37mSALIR"
echo -e "$BARRA"
while [[ ${varread} != @([0-9]) ]]; do
read -p "⟬Opcion⟭ ➤ " varread
done
echo -e "$BARRA"
if [[ ${varread} = 0 ]]; then
exit
elif [[ ${varread} = 1 ]]; then
gerar_key
elif [[ ${varread} = 2 ]]; then
remover_key
elif [[ ${varread} = 3 ]]; then
echo -e "\033[1;33mINTENTOS MULTI-LOGIN IP FIJA\033[0m"
echo -e "$BARRA"
echo -ne "\033[1;37m"
cat /etc/gerar-sh-log 2>/dev/null || echo "NO SE HA GENERADO LOG"
echo -ne "\033[0m" && read -p "Enter"
elif [[ ${varread} = 4 ]]; then
remover_key_usada
elif [[ ${varread} = 5 ]]; then
start_gen
elif [[ ${varread} = 6 ]]; then
atualizar_keyfixa
elif [[ ${varread} = 7 ]]; then
atualizar_geb
elif [[ ${varread} = 8 ]]; then
uninstal_master
elif [[ ${varread} = 9 ]]; then
bot_menu
fi
/usr/bin/gerar.sh